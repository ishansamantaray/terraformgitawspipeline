FROM openjdk:latest
LABEL maintainer="ishansamantaray@outlook.com"
VOLUME /tmp
ADD build/libs/aws-spring-boot-demo.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]
EXPOSE 9000